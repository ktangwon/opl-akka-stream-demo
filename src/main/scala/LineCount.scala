object LineCount {
  def main(args: Array[String]): Unit = {
    val startTime = System.nanoTime
    val lines = io.Source.fromFile("src/main/resources/2008.csv", "utf-8")
                  .getLines

    var lineCount = 0

    lines.foreach { _ => lineCount += 1 }

    println(lineCount)

    val finishTime = System.nanoTime
    println(s"elapsed: ${(finishTime - startTime)/1e9}")
 
  }
}

