import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import scala.util.Try
import akka.stream.scaladsl.Sink
import akka.stream.ClosedShape

import scala.util.Success
import scala.concurrent.Future


object AkkaChunkedStreamProcessor {

  import scala.concurrent.ExecutionContext.Implicits._

  sealed case class FlightRecord(carrierCode: String, arrDelay: Int)

  implicit val system = ActorSystem("Sys")
  implicit val materializer = ActorMaterializer()

  def main(args: Array[String]): Unit = {
    val startTime = System.nanoTime
    val GROUP_SIZE = 16384
    val g = RunnableGraph.fromGraph(GraphDSL.create() {
      implicit gb => // graph builder
        import GraphDSL.Implicits._

        val srcLines = gb.add(Source.fromIterator(() => {
          io.Source.fromFile("src/main/resources/2008.csv", "utf-8")
                        .getLines
                        .grouped(GROUP_SIZE)
        }))

        val parseSelectDelayed = gb.add(Flow[Seq[String]]
          .mapAsyncUnordered(parallelism = 24){ r => parseGroup(r) }
          .mapConcat( r => r )
        )

        val groupStatsSubFlows = Flow[FlightRecord]
          .groupBy(maxSubstreams = 128, _.carrierCode)
          .fold(("", 0, 0)) { case ((_, count, sum), y) => {
            val (count_, sum_) = (count + 1, sum + y.arrDelay)

            (y.carrierCode, count_, sum_)
            }
          }

        val groupStatsFlow = groupStatsSubFlows
          .mergeSubstreams
          .map { case (code, n, sumDelay) => (code, n, sumDelay.toDouble/n) }

        val groupStats = gb.add(groupStatsFlow)

        val reportPrinting = gb.add(
          Flow[(String, Int, Double)]
            .map { r =>
              println(r)

              r
            }
        )

        val finishTimingSink = gb.add(Sink.onComplete {
          case Success(v) => {
            val stopTime = System.nanoTime
            println(s"elapsed: ${(stopTime - startTime)/1e9}ms")

            system.terminate()
          }
        })
        srcLines.out ~>
          parseSelectDelayed ~>
          groupStats ~>
          reportPrinting ~>
          finishTimingSink

        ClosedShape
    })


    g.run()
  }

  def parseGroup(lines: Seq[String]) = Future {
    val flightRecords = lines.map { line =>
      val attributes = line.split(",")
      val (carrier, arrDelay) = (attributes(8), attributes(14))
      FlightRecord(carrier, Try(arrDelay.toInt).getOrElse(-1))
    }
    val delayedRecords = flightRecords.filter(_.arrDelay > 0).toVector

    delayedRecords
  }
}

