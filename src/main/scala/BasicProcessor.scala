import scala.util.Try


sealed case class FlightRecord(carrierCode: String, arrDelay: Int)
object BasicProcessor {
  def main(args: Array[String]): Unit = {
    val startTime = System.nanoTime
    val lines = io.Source.fromFile("src/main/resources/2008.csv", "utf-8")
                  .getLines

    val flightRecords = lines.map { line =>
      val attributes = line.split(",")
      val (carrier, arrDelay) = (attributes(8), attributes(14))
      FlightRecord(carrier, Try(arrDelay.toInt).getOrElse(-1))
    }
    val delayedRecords = flightRecords.filter(_.arrDelay > 0).toSeq

    val reportCards = delayedRecords
      .groupBy(_.carrierCode)
      .mapValues { carrierRecords =>
        val n = carrierRecords.length
        val sumDelay = carrierRecords.map(_.arrDelay).sum
        (n, sumDelay.toDouble/n)
      }

    reportCards.foreach { case (code, (count, avgDelay)) =>
      println(s"Delays for carrier ${code}: ${avgDelay} average mins, ${count} delayed flights")
    }
    val finishTime = System.nanoTime
    println(s"elapsed: ${(finishTime - startTime)/1e9}")
  }
}

