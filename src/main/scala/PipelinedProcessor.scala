import scala.util.Try
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await

object PipelinedProcessor {
  sealed case class FlightRecord(carrierCode: String, arrDelay: Int)
  val GROUP_SIZE = 16384

  def parseGroup(lines: Seq[String]) = Future {
    val flightRecords = lines.map { line =>
      val attributes = line.split(",")
      val (carrier, arrDelay) = (attributes(8), attributes(14))
      FlightRecord(carrier, Try(arrDelay.toInt).getOrElse(-1))
    }
    val delayedRecords = flightRecords.filter(_.arrDelay > 0).toSeq

    delayedRecords
  }

  def main(args: Array[String]): Unit = {
    val startTime = System.nanoTime
    val lines = io.Source.fromFile("src/main/resources/2008.csv", "utf-8")
                  .getLines

    val delayGroups = lines
      .grouped(GROUP_SIZE)
      .map(parseGroup)

    val reportCards = Future.sequence(delayGroups)
      .map { _.flatten
         .toSeq
         .groupBy(_.carrierCode)
         .mapValues { carrierRecords =>
           val n = carrierRecords.length
           val sumDelay = carrierRecords.map(_.arrDelay).sum
           (n, sumDelay.toDouble/n)
         }
      }
    val report = Await.result(reportCards, Duration.Inf)
    report.foreach { case (code, (count, avgDelay)) =>
      println(s"Delays for carrier ${code}: ${avgDelay} average mins, ${count} delayed flights")
    }

    val finishTime = System.nanoTime
    println(s"elapsed: ${(finishTime - startTime)/1e9}")
  }
}

