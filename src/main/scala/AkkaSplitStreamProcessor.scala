import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import scala.util.Try
import akka.stream.scaladsl.Sink
import akka.stream.ClosedShape
import akka.stream.Inlet
import scala.util.Success
import scala.concurrent.Future


object AkkaSplitStreamProcessor {

  import scala.concurrent.ExecutionContext.Implicits._

  sealed case class FlightRecord(carrierCode: String, arrDelay: Int)

  implicit val system = ActorSystem("Sys")
  implicit val materializer = ActorMaterializer()

  def main(args: Array[String]): Unit = {
    val startTime = System.nanoTime
    val g = RunnableGraph.fromGraph(GraphDSL.create() {
      implicit gb => // graph builder
        import GraphDSL.Implicits._

        val srcLines = gb.add(Source.fromIterator(() => {
          io.Source.fromFile("src/main/resources/2008.csv", "utf-8")
                        .getLines
        }))

        val slaveCount = 4

        val balancer = gb.add(Balance[String](slaveCount))
        val merge =  gb.add(Merge[FlightRecord](slaveCount))

        val parseRecordFilterFlow = Flow[String].map { line =>
          val attributes = line.split(",")
          val (carrier, arrDelay) = (attributes(8), attributes(14))
          FlightRecord(carrier, Try(arrDelay.toInt).getOrElse(-1))
        }
        .filter(_.arrDelay > 0)

        (0 until slaveCount).foreach { flowNo =>
          balancer.out(flowNo) ~> parseRecordFilterFlow.async ~> merge.in(flowNo)
        }


        val groupStatsSubFlows = Flow[FlightRecord]
          .groupBy(maxSubstreams = 128, _.carrierCode)
          .fold(("", 0, 0)) { case ((_, count, sum), y) => {
            val (count_, sum_) = (count + 1, sum + y.arrDelay)

            (y.carrierCode, count_, sum_)
            }
          }

        val groupStatsFlow = groupStatsSubFlows
          .mergeSubstreams
          .map { case (code, n, sumDelay) => (code, n, sumDelay.toDouble/n) }

        val groupStats = gb.add(groupStatsFlow)

        val reportPrinting = gb.add(
          Flow[(String, Int, Double)]
            .map { r =>
              println(r)

              r
            }
        )
        val finishTimingSink = gb.add(Sink.onComplete {
          case Success(v) => {
            val stopTime = System.nanoTime
            println(s"elapsed: ${(stopTime - startTime)/1e9}ms")

            system.terminate()
          }
        })

        srcLines.out ~> balancer
        merge ~>
          groupStats ~>
          reportPrinting ~>
          finishTimingSink.in

        ClosedShape
    })

    g.run()
  }
}

