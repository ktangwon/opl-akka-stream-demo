
name := """flight-delay-scala"""

version := "1.1"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % "2.4.12",
  "com.typesafe.akka" %% "akka-actor" % "2.4.12",
  "org.apache.commons" % "commons-compress" % "1.12",
  "com.chuusai" %% "shapeless" % "2.3.2"
)

